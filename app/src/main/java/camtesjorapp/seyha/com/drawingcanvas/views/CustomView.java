package camtesjorapp.seyha.com.drawingcanvas.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import camtesjorapp.seyha.com.drawingcanvas.R;

/**
 * Created by mac on 1/16/2018 AD.
 */

public class CustomView extends View{

    private Paint mCirclePaint;
    private Rect mCircleRect;
    private final int SQUARE_SIZE_DEF = 200;
    private int mSquareColor;
    private float mSquareSize;
    private float mCirclePosX;
    private float mCirclePosY;

    public CustomView(Context context) {
        super(context);
        init(null);
    }

    public CustomView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public CustomView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    public CustomView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(attrs);
    }

    private void init(@Nullable AttributeSet attrs){

        mCirclePaint = new Paint(Paint.ANTI_ALIAS_FLAG);

        mCircleRect = new Rect();

        if(attrs == null) return;

        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.CustomView);
        mSquareColor = typedArray.getColor(R.styleable.CustomView_square_color, Color.RED);
        mSquareSize = typedArray.getDimensionPixelSize(R.styleable.CustomView_square_size,SQUARE_SIZE_DEF);
        mCirclePaint.setColor(mSquareColor);
        typedArray.recycle();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if(mCirclePosX == 0 || mCirclePosY == 0){
            mCirclePosX = getWidth()/2;
            mCirclePosY = getHeight()/2;
        }

        canvas.drawCircle(mCirclePosX,mCirclePosY,300,mCirclePaint);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        boolean isTouch = super.onTouchEvent(event);

        switch (event.getAction()){
            case MotionEvent.ACTION_DOWN:


                return true;
            case MotionEvent.ACTION_MOVE:

                float x = event.getX();
                float y = event.getY();

                Log.d("=","=====================");

                Log.d("XY: ","X:"+ x +"|"+"Y:"+y);

                Log.d("CirPosXY:","cirPosX: "+mCirclePosX+" cirPosY: " + mCirclePosY);

                Log.d("Touch and Post","Minus x: "+(x - mCirclePosX) +" : Minus y: "+(y - mCirclePosY));

                double dalX = Math.pow(x - mCirclePosX,2);
                double dalY = Math.pow(y - mCirclePosY,2);

                Log.d("DelXY: ","DelX:"+ dalX +"|"+"DelY:"+dalY);

                if(dalX + dalY < Math.pow(300,2)){

                    mCirclePosX = x;
                    mCirclePosY = y;

                    postInvalidate();

                    return true;
                }

                return isTouch;
        }

        return isTouch;
    }
}
